import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TeamService } from '../../providers/team.service';
import { Team } from '../../models/team';
import { PlayerPage } from '../player/player';
/*
  Generated class for the TeamList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-team-list',
  providers: [TeamService],
  templateUrl: 'team-list.html'
})
export class TeamList {

  teams: Array<any>;
  constructor(public navCtrl: NavController, public teamservice: TeamService) {}

  ngOnInit() {
    this.teamservice.getAllTeams().subscribe(data => {console.log(data); this.teams = data}, err => console.log(err), () => console.log('hey I did something'));
    console.log(this.teams);
  }
  ionViewDidLoad() {
    console.log('Hello TeamList Page');
  }
  itemTapped(event, team) {
    this.navCtrl.push(PlayerPage, {
        team: team
    })
  }

}

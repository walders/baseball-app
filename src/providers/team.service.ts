import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Team } from '../models/team';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TeamService {
    static get parameters() {
        return [[Http]];
    };
    constructor (private http: Http) {}
    
    private teamUrl = 'https://typescripttest-walders.c9users.io/api/teams';
    
    getAllTeams(): Observable <Team[]> {
        var url = this.teamUrl;
        return this.http.get(url).map(res => res.json().data);
    }
    
}
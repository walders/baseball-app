import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { PlayerPage} from '../pages/player/player';
import { PlayerInfo} from '../pages/player-info/player-info';
import { TeamPage } from '../pages/team/team';
import { TeamList } from '../pages/team-list/team-list';
import { PitchCount } from '../pages/pitch-count/pitch-count';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    PlayerPage,
    PlayerInfo,
    TeamPage,
    TeamList,
    PitchCount
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    PlayerPage,
    PlayerInfo,
    TeamPage,
    TeamList,
    PitchCount
  ],
  providers: []
})
export class AppModule {}

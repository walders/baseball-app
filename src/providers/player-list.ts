import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

//Import the player model
import {Player} from '../../models/player'
/*
  Generated class for the PlayerList provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PlayerList {
  playerlist: any=null;
  constructor(public http: Http) {
    console.log('Hello PlayerList Provider');
  }
  
  load() {
    if (this.playerlist) {
      // already loaded users
      return Promise.resolve(this.playerlist);
    }

    // don't have the users yet
    return new Promise(resolve => {
      // We're using Angular Http provider to request the users,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the users and resolve the promise with the new data.
      this.http.get('https://typescripttest-walders.c9users.io/api/players')
        .map(res => <Array<Player>>(res.json()))
        .subscribe(player => {
          // we've got back the raw users, now generate the core schedule users
          // and save the users for later reference
          this.playerlist = players;
          resolve(this.playerlist);
        });
    });
  }
}

import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Player } from '../models/player';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PlayerService {
    static get parameters() {
        return [[Http]];
    };
    constructor (private http: Http) {}
    
    private playerUrl = 'https://typescripttest-walders.c9users.io/api/players';
    
    getAllPlayers(): Observable <Player[]> {
        var url = this.playerUrl;
        return this.http.get(url).map(res => res.json().data);
          
    }
    
    getPlayersByTeam(team_id) {
        let url = this.playerUrl + '/team/' + team_id;
        return this.http.get(url).map(res => res.json().data);
    }
    
    updatePlayer(player: Player) {
        let url = this.playerUrl + '/' + player.id;
        let body = JSON.stringify(player);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put(url, body, options)
                    .map(this.extractData)
                    .catch(this.handleError);
    }
    
    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
      }
      private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
      }
    /*
    getPlayers: Promise <Player[]> {
       return this.http.get(this.playerUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }
    */
}

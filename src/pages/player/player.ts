import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PlayerService } from '../../providers/player.service';
import { Player } from '../../models/player';
import { PlayerInfo } from '../player-info/player-info';
import { Team } from '../../models/team';
/*
  Generated class for the Player page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-player',
  providers: [PlayerService],
  templateUrl: 'player.html',
  entryComponents: [PlayerInfo]
})
export class PlayerPage {
  
  players: Array<any>;
  team: Team;
  constructor(public navCtrl: NavController, public playerservice: PlayerService, private navparams: NavParams) {
    this.team = navparams.get('team');
    console.log(this.team);
  }


  ngOnInit() {
    if (this.team) {
      this.playerservice.getPlayersByTeam(this.team.id).subscribe(data => {console.log(data); this.players = data}, err => console.log(err), () => console.log('hey I did something'));
    } else {
      this.playerservice.getAllPlayers().subscribe(data => {console.log(data); this.players = data}, err => console.log(err), () => console.log('hey I did something'));  
    }
  }
  ionViewDidLoad() {
    console.log('Hello Player Page');
  }
  getAverage(player) {
    if (player.at_bats === 0) {
      return '.000';
    }
    var average = player.hits/player.at_bats;

    var avg = average.toFixed(3).replace(/\b0+/, '')
    return avg;
  }
  itemTapped(event, player) {
    this.navCtrl.push(PlayerInfo, {
            player: player
    });
  }
  
  createPlayer() {
    this.navCtrl.push(PlayerInfo, {
            player: {}
    });
  }

}

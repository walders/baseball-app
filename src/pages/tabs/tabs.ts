import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ContactPage } from '../contact/contact';
import { PlayerPage } from '../player/player';
import { TeamList } from '../team-list/team-list';
import { PitchCount } from '../pitch-count/pitch-count';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = TeamList;
  tab3Root: any = PlayerPage;
  tab4Root: any = PitchCount;
  constructor() {

  }
}

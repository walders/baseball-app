import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the PitchCount page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pitch-count',
  templateUrl: 'pitch-count.html'
})
export class PitchCount {
  public pitches: number = 0;
  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello PitchCount Page');
  }
  
  itemTapped(e) {
    this.pitches++;
  }

}

export class Player {
    player_id: number;
    first_name: string;
    last_name: string;
    team_id: number;
    hits: number;
    walks: number;
    at_bats: number;
    stolen_base: number;
}
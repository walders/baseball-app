import { Component } from '@angular/core';
import {Validators, FormBuilder } from '@angular/forms';
import {TeamService} from '../../providers/team.service';
/*
  Generated class for the Team page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-team',
  providers: [TeamService],
  templateUrl: 'team.html'
})
export class TeamPage {

  private formBuilder: FormBuilder {}
  
  constructor(fb: FormBuilder) {
    this.team = fb.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9 ]*')])],
    });
  }
  
  form: FormGroup;
  logForm(){
    console.log(this.team.value)
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { Player } from '../../models/player';
import { PlayerService } from '../../providers/player.service';

/*
  Generated class for the PlayerInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-player-info',
  providers: [PlayerService],
  templateUrl: 'player-info.html'
})
export class PlayerInfo {
    player: Player;
  constructor(public navCtrl: NavController, private navParams: NavParams, private formBuilder: FormBuilder, private playerservice: PlayerService) {
      this.player = navParams.get('player');
      console.log('construction complete');
  }

  ionViewDidLoad() {
    console.log(this.player);

  }
  
  savePlayer(){
    console.log(this.player);
    this.playerservice.updatePlayer(this.player).subscribe(
                       data  => this.navCtrl.pop();,
                       error => console.log(error));

  }
}
